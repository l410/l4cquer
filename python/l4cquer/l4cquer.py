from collections import deque
from enum import Enum

class DefaultASCIICategorizer:
    def __getitem__(self, key):
        if key in '"`\'':
            return key
        elif key in ' \t\v':
            return ' '
        elif key in '\n\r':
            return '\n'
        elif key in '\\':
            return '\\'
        elif key in 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_':
            return 'a'
        else:
            return '?'


def tokenize(data, categorizer=DefaultASCIICategorizer()):
    """
    A generator for tokenizing data according to the L4CQUER standard.

    The categorizer passed should provide __getitem__ such that passing any
    element of data as argument will return a "standard" string to indicate its
    class. These are:
        "  Primary quotation
        '  Secondary quotation
        `  Tertiary quotation
           Whitespace
        \n End of line
        \\ Comment

    Any other return value defines its own other class.

    Note that for strings you may wish to call ''.join(<>) on the result.

    :param data: The data to tokenize
    :param categorizer: The categorizer
    :returns: A generator for each token as a list, ending with an empty list.
    """
    previous_eol = False
    items = iter(data)
    peek = []

    def _block_comment(init_size):
        size = init_size
        initial = True
        close = 0

        for item in items:
            category = categorizer[item]
            if category == '\\':
                if initial:
                    size += 1
                else:
                    close += 1
                    if close == size:
                        return True
            else:
                initial = False
                close = 0

        return False

    def _line_comment():
        try:
            followup = next(items)
            category = categorizer[followup]

            if category == '\\':
                return _block_comment(2)
            else:
                for item in items:
                    if categorizer[item] == '\n':
                        return True
                return True

        except StopIteration:
            return False

    def _quoted(start, level):
        size = 1
        initial = True
        close = 0

        token = start

        for item in items:
            category = categorizer[item]
            token += [item]
            if category == level:
                if initial:
                    size += 1
                else:
                    close += 1
                    if close == size:
                        return token
            else:
                if initial:
                    initial = False
                    if size % 2 == 0:
                        peek.append((item, category))
                        return token
                else:
                    close = 0

        return token

    def _bare(start, identifier):
        token = start

        for item in items:
            category = categorizer[item]
            if category == identifier:
                token += [item]
            else:
                peek.append((item, category))
                return token

        return token

    try:
        while True:
            if peek:
                item, category = peek.pop(0)
            else:
                item = next(items)
                category = categorizer[item]

            if category == '\\':
                if not _line_comment():
                    return # No EOL
            elif category == '\n':
                if not previous_eol:
                    yield [item]
                previous_eol = True
            elif category in '"\'`':
                yield _quoted([item], category)
                previous_eol = False
            elif category != ' ':
                yield _bare([item], category)
                previous_eol = False
    except StopIteration:
        yield []

