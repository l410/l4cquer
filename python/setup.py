from setuptools import find_packages, setup

setup(
    name='l4cquer',
    packages=find_packages(),
    version='0.1.0',
    description='L4CQUER Python Implementation',
    author='Louis A. Burke',
)
