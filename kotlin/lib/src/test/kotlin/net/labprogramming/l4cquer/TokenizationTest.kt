package net.labprogramming.l4cquer

import kotlin.test.*

class TokenizationTest {
    object IdentifierCharacter : L4CQUERCategory.Other()
    object SymbolicCharacter : L4CQUERCategory.Other()

    private val asciiL4CQUER = L4CQUER<Char> {
        when (it) {
            '"' -> L4CQUERCategory.Quotation.PrimaryQuotation
            '\'' -> L4CQUERCategory.Quotation.SecondaryQuotation
            '`' -> L4CQUERCategory.Quotation.TertiaryQuotation
            ' ' -> L4CQUERCategory.Whitespace
            '\n' -> L4CQUERCategory.EndOfLine
            '\\' -> L4CQUERCategory.Comment
            in "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz_" -> IdentifierCharacter
            else -> SymbolicCharacter
        }
    }

    private fun L4CQUER<Char>.assertTokenizesAs(input : String, vararg expected : String) {
        val output = tokenize(input.asSequence())

        for ((o, e) in output.zip(expected.asSequence())) {
            assert(o.last in input.indices) { "$o not in ${input.indices} ($input - $e)"}
            assert(o.first in input.indices) { "$o not in ${input.indices} ($input - $e)"}
            assertEquals(e, input.substring(o))
        }
    }

    @Test fun `Basic tokenization test`() {
        with(asciiL4CQUER) {
            assertTokenizesAs("test 'with' `strings`", "test", "'with'", "`strings`", "")
            assertTokenizesAs("test \\ with comment\n", "test", "")
        }
    }

    @Test fun `Commented EOF test`() {
        with(asciiL4CQUER) {
            assertTokenizesAs("\\")
            assertTokenizesAs("test \\\\\n\\ still comment\n\n", "test")
        }
    }

    @Test fun `Empty strings test`() {
        with(asciiL4CQUER) {
            assertTokenizesAs("\"\" '' `` \"", "\"\"", "''", "``", "\"", "")
        }
    }

    @Test fun `Fancy strings test`() {
        with(asciiL4CQUER) {
            assertTokenizesAs(""" '''he said "it's mine!"''' """, """'''he said "it's mine!"'''""", "")
            assertTokenizesAs(""" "" "" a "" '' `` ```''''''''''```''""```'``` """, "\"\"", "\"\"", "a", "\"\"", "''", "``", "```''''''''''```", "''", "\"\"", "```'```", "")
            assertTokenizesAs(" '''\u0000\u0000\u0000''' ", "'''\u0000\u0000\u0000'''", "")
        }
    }
}