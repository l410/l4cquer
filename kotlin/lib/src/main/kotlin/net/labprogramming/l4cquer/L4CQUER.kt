package net.labprogramming.l4cquer

class L4CQUER<T>(val categorizer : (T) -> L4CQUERCategory) {
    // Note: Using mutually tail-recursive functions for each state would be cleaner, but less efficient in Kotlin
    private sealed class TokenizerState {
        abstract val current : Int // This represents the index of the next consumed value
        abstract suspend fun SequenceScope<IntRange>.consume(input : L4CQUERCategory) : TokenizerState
        open suspend fun SequenceScope<IntRange>.end() { yield(IntRange.EMPTY) }
        val next get() = current + 1

        suspend fun SequenceScope<IntRange>.afterNext(input : L4CQUERCategory) : TokenizerState =
            when (input) {
                L4CQUERCategory.Comment -> LineComment(next, false)
                L4CQUERCategory.Whitespace -> Empty(next)
                L4CQUERCategory.EndOfLine -> {
                    yield(current .. current)
                    Empty(next, true)
                }
                is L4CQUERCategory.Other -> Bare(next, input)
                is L4CQUERCategory.Quotation -> Quoted(next, input)
            }

        data class Empty(
            override val current : Int = 0,
            val previousEOL : Boolean = false
        ) : TokenizerState() {
            override suspend fun SequenceScope<IntRange>.consume(input: L4CQUERCategory) : TokenizerState = when (input) {
                L4CQUERCategory.Comment -> LineComment(next, previousEOL)
                L4CQUERCategory.Whitespace -> Empty(next, previousEOL)
                L4CQUERCategory.EndOfLine -> {
                    if (!previousEOL)
                        yield(current .. current)
                    Empty(next, true)
                }
                is L4CQUERCategory.Other -> Bare(next, input)
                is L4CQUERCategory.Quotation -> Quoted(next, input)
            }
        }

        data class LineComment(
            override val current : Int,
            val previousEOL : Boolean,
            val initial : Boolean = true
        ) : TokenizerState() {
            override suspend fun SequenceScope<IntRange>.end() { }

            override suspend fun SequenceScope<IntRange>.consume(input: L4CQUERCategory): TokenizerState = when (input) {
                L4CQUERCategory.Comment -> if (initial) BlockComment(next, previousEOL) else copy(current = next)
                L4CQUERCategory.EndOfLine -> Empty(next, previousEOL)
                else -> LineComment(next, previousEOL, false)
            }
        }

        data class BlockComment(
            override val current: Int,
            val previousEOL : Boolean,
            val size : Int = 2,
            val initial : Boolean = true,
            val closing : Int = 0
        ) : TokenizerState() {
            override suspend fun SequenceScope<IntRange>.end() { }

            override suspend fun SequenceScope<IntRange>.consume(input: L4CQUERCategory): TokenizerState = when (input) {
                L4CQUERCategory.Comment ->
                    when {
                        initial -> BlockComment(next, previousEOL, size + 1, true)
                        closing == size - 1 -> Empty(next, previousEOL)
                        else -> BlockComment(next, previousEOL, size, false, closing + 1)
                    }
                else -> BlockComment(next, previousEOL, size, false, 0)
            }
        }

        data class Bare(
            override val current: Int,
            val category : L4CQUERCategory.Other,
            val start : Int = current - 1
        ) : TokenizerState() {
            override suspend fun SequenceScope<IntRange>.consume(input: L4CQUERCategory): TokenizerState = when (input) {
                category -> copy(current = next)
                else -> {
                    yield(start until current)
                    afterNext(input)
                }
            }
        }

        data class Quoted(
            override val current: Int,
            val category : L4CQUERCategory.Quotation,
            val size : Int = 1,
            val start : Int = current - 1,
            val initial : Boolean = true,
            val closing : Int = 0
        ) : TokenizerState() {
            override suspend fun SequenceScope<IntRange>.end() {
                yield(start until current)
                yield(IntRange.EMPTY)
            }

            override suspend fun SequenceScope<IntRange>.consume(input: L4CQUERCategory): TokenizerState = when (input) {
                category ->
                    when {
                        initial -> copy(current = next, size = size + 1)
                        closing == size - 1 -> {
                            yield(start .. current)
                            Empty(next, false)
                        }
                        else -> copy(current = next, closing = closing + 1)
                    }

                else ->
                    if (initial && size % 2 == 0) {
                        yield(start until current)
                        afterNext(input)
                    } else {
                        copy(current = next, initial = false, closing = 0)
                    }
            }
        }
    }

    fun tokenize(input : Sequence<T>) : Sequence<IntRange> = sequence {
        var state : TokenizerState = TokenizerState.Empty()

        for (item in input)
            state = state.run { consume(categorizer(item)) }

        state.run { end() }
    }

    fun unquote(input : Sequence<T>) : IntRange {
        TODO()
    }

    fun quote(input : Sequence<T>) : Sequence<T> {
        TODO()
    }
}
