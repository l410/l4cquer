package net.labprogramming.l4cquer

sealed class L4CQUERCategory {
    sealed class Quotation : L4CQUERCategory() {
        object PrimaryQuotation : Quotation()
        object SecondaryQuotation : Quotation()
        object TertiaryQuotation : Quotation()
    }
    object Whitespace : L4CQUERCategory()
    object EndOfLine : L4CQUERCategory()
    object Comment : L4CQUERCategory()
    open class Other : L4CQUERCategory()
}