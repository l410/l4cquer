% arara: xelatex
% vim: textwidth=0 wrap spell spelllang=en_ca
\documentclass{l4}
\usepackage{l4pseudocode}

\usepackage{textcomp}
\usepackage[toc,page]{appendix}

\author{Louis A. Burke}
\title{L4CQUER Standard}
\version{0.0.1}

\newcommand{\shall}{\textsc{shall}}
\newcommand{\shallnt}{\textsc{shall not}}
\newcommand{\should}{\textsc{should}}
\newcommand{\shouldnt}{\textsc{should not}}
\newcommand{\could}{\textsc{could}}
\newcommand{\will}{\textsc{will}}
\newcommand{\willnt}{\textsc{will not}}

\colorlet{hlred}{red!30}
\colorlet{hlgreen}{green!30}

\begin{document}

\section{Introduction}

A L4CQUER \index{L4CQUER} token is an abbreviation for an L4 Contiguous Quoted Universally Encoded Raw token. This specification describes the interpretation of an input stream as a stream of L4CQUER tokens. The purpose of L4CQUER tokenization is to ensure consistent representation of raw data. Additionally, it makes tokenization possible with constant memory requirements and linear time requirements. Finally, the representation ensures that the internal values of all tokens are represented as contiguous sections of the input stream. L4CQUER is intended to be used as a tokenization strategy for more advanced data encodings.

\subsection{Conventions Used in This Document}

In this specification, ``\shall'' is interpreted as a requirement on an implementation; conversely ``\shallnt'' is interpreted as a prohibition.  Similarly ``\should'' is interpreted as a suggestion; conversely ``\shouldnt'' is interpreted as a recommendation to avoid. The term ``\could'' is interpreted as a choice an implementation may make while still conforming to this specification. Finally, ``\will'' is interpreted as an inevitable situation that an implementation encounters; conversely ``\willnt'' is interpreted as an impossible situation for an implementation to encounter.

\section{Scope}

L4CQUER is a lightweight, flexible, text-based, language-independent syntax for defining a stream of raw data tokens.

The goal of this specification is only to define the syntax of valid L4CQUER token streams. It does not intend to provide any semantics of said streams of tokens.

\section{Terms, definitions, and symbols}

For the purpose of this specification, the following definitions apply. Terms explicitly defined here are not to be presumed to refer implicitly to similar terms defined elsewhere. Terms not defined in this specification are to be interpreted according to accepted common English usage.

\subsection{Character}

\definition{character}{a member of a set of elements used for the organization, control, or representation of data.}

Characters may be bytes, Unicode code points, or any other encoding with the required flexibility. It is common to specify the character encoding method along with this encoding specification. For example, one might specify a stream of data as being formatted as UTF8-L4CQUER if it uses the UTF-8 encoding of the ISO/IEC 10646 standard for the underlying characters.

\subsection{Stream and Sequence}

\definition{stream}{an ordered flow of stream elements of unspecified length.}

\definition{sequence}{an ordered series of elements of unspecified length.}

Streams and sequences are used very similarly in this specification. The difference is that a stream is a dynamic flow of data, while a sequence is a static specification of data.

In this specification is stream is said to consist of the sequence of items that flow through it.

\section{Conformance}

A conforming L4CQUER stream \shall\ consist of a sequence of characters that conforms to the L4CQUER stream description defined in this specification.

A conforming processor of L4CQUER streams \shall\ not accept any inputs that do not conform to this specification. A conforming processor of L4CQUER streams \shall\ output the same L4CQUER tokens as the procedure defined in this specification. The output of a conforming processor \shall\ be in the same order as specified in this specification. A conforming processor \could\ impose additional semantic restrictions that limit the set of conforming inputs that it will process.

\section{Syntactic Categories}

A L4CQUER stream \will\ consist of a sequence of characters. Each character \shall\ be a member of one syntactic category. A processor of L4CQUER streams \should\ specify which syntactic category any character is a member of. A processor of L4CQUER streams \could\ also allow the category of characters to be configured, perhaps even dynamically.

The syntactic categories to which characters \shall\ belong are:

\begin{itemize}
    \item[\textquotedbl] Primary Quotation Mark: There \should\ be at least one character whose category is Primary Quotation Mark. These characters \will\ represent the most preferred quotation marks for strings.

    \item[\textquotesingle] Secondary Quotation Mark: There \should\ be a at least one character whose category is Secondary Quotation Mark. These characters \will\ represent the second most preferred quotation marks for strings.

    \item[\textasciigrave] Tertiary Quotation Mark: There \should\ be at least one character whose category is Tertiary Quotation Mark. These characters \will\ represent the third most preferred quotation marks for strings.

    \item[\textvisiblespace] Whitespace: There \should\ be some characters whose category is Whitespace. These characters \will\ represent horizontal whitespace and are used to separate tokens.

    \item[\textparagraph] End Of Line: There \should\ be some characters whose category is End Of Line. These characters \will\ represent the end of a line of text.

    \item[\textbackslash] Comment: There \should\ be some characters whose category is Comment. These characters \will\ represent a separation from text to be processed.

    \item[?] Other: Any character that is not a member of one of the above categories \shall\ be a member of any number of additional categories. Characters in the same family will be grouped into contiguous tokens. A conforming implementation \should\ support enough categories such that each character may belong to its own unique category if necessary. A conforming implementation \shall\ support at least ten additional categories (for a total of at least sixteen categories).
\end{itemize}

\section{L4CQUER Tokens}

An input which conforms to this specification \shall\ consist of a series of L4CQUER tokens.

Each token consumes some prefix of the input stream and represents a contiguous sub-sequence thereof. The only necessary data to be described by a token is the continuous sub-sequence of input it represents. This sequence \will\ only be empty when the token is signalling the end of data. Otherwise, the content of the token \will\ be enough information to determine the type of the token. As a consequence of this, a quoted string and a bare string of the same content may not be distinguishable in the token stream.

A conforming processor of L4CQUER streams \shall\ output the sequence of tokens produced by converting the entire input into tokens, in order. \index{token}

\subsection{Comments}

L4CQUER supports comments. These take two forms. They are described here along with examples which use ``\textbackslash'' as a comment character. End of line characters are explicitly represented as ``\textparagraph''. Comments are highlighted in green while non-comments are highlighted in red.

\subsubsection{Line Comment}

A single comment character followed by any other character except for an end of line character begins a line comment. The rest of the input until either an end of line character or the end of the input \shall\ be discarded. Note that this includes the final character, be it the end of line character or the end of the input. As such a line comment can be used to make lines ``continue'' after a line break.

\begin{rawcode}
    %     \ this is a comment \ still a comment \\\ still just a line comment
\hlc{hlred}{Not a comment, }\hlc{hlgreen}{\textbackslash this is a comment \textbackslash\ still a comment \textbackslash\textbackslash\textbackslash\ still just a line comment \textparagraph} \\
    % Not a comment, comment as line \
\hlc{hlred}{Not a comment, comment as line }\hlc{hlgreen}{\textbackslash\textparagraph} \\
    % continuation, comment as end of file removal (may cause unwanted behaviour in some applications) \
\hlc{hlred}{continuation, comment as end of file removal}\hlc{hlgreen}{\textbackslash} \\
\end{rawcode}

\subsubsection{Block Comment}

Two or more comment characters followed by any other character begin a block comment. All characters until a sequence of the same comment characters that began the comment \shall\ be discarded. If the input ends before that same sequence is encountered than the end of the file \shall\ also be discarded.

\begin{rawcode}
    % \\ this is a comment, \ still a block comment
\hlc{hlgreen}{\textbackslash\textbackslash this is a comment, \textbackslash\ still a block comment\textparagraph} \\
    % Still a comment \\ not a comment anymore \\\ more comment
\hlc{hlgreen}{Still a comment \textbackslash\textbackslash}\hlc{hlred}{\ not a comment anymore\ }\hlc{hlgreen}{\textbackslash\textbackslash\textbackslash\ more comment\textparagraph} \\
    % Still comment \\\\\ new comment, still discarded, end of file removed
\hlc{hlgreen}{Still comment \textbackslash\textbackslash\textbackslash\textbackslash\textbackslash\ new comment, still discarded, end of file removed} \\
\end{rawcode}

\subsection{Whitespace}

In this specification whitespace specifically refers to token separation whitespace, typically spaces and tabs. However, this section also describes tokens that do not encode visible characters in the input.

\subsubsection{End of Lines}

If an end of line character is not discarded by a comment and the previously output token was not an end of line token, then the next token \shall\ be an end of line token, containing just the specific end of line character that was consumed.

\subsubsection{End of Data}

If the input data ends and is not discarded by a comment, then the next and final token \shall\ be an end of data token, containing no characters whatsoever.

\subsection{Bare Strings}

Each contiguous region of input comprised of a single ``other'' category code \shall\ be consumed as a single token. Note that what differentiates whitespace characters from other characters is that they separate tokens without themselves being output as a token.

\subsection{Quoted Strings}

The quoted strings that give L4CQUER its name are formed by repeated instances of a quotation character. The entire sequence of characters from the first that is in a different category from the initial quotation character until the same sequence of quotation characters or the end of input \shall\ constitute the resulting output token. Only the category of the quotation characters must match. If there are an even number of quotation marks of the same category in a row, followed by any other category, then they represent an empty string and are returned as a single token consisting purely of quotation characters of a single category.

\section{Canonical Encoding}

With the three quotation categories available, any string can be unambiguously quoted. When generating L4CQUER data, there is a canonical encoding that may be used. This encoding uses the fewest possible quotation marks while prioritizing primary quotation marks over secondary, and secondary quotation marks over tertiary.

% TODO: pseudocode for canonical encoding

\section{Standard Predefined Categories}

There are some common encoding categories that are useful to define as part of this specification. This allows for shorter descriptions in other specifications.

\subsection{L4CQUER ASCII}

% TODO: ASCII

\subsection{L4CQUER UNICODE}

% TODO: UNICODE

\begin{appendices}
\section{Pseudo-code Implementation}

Here is a basic pseudo-code implementation for a conforming generator of L4CQUER tokens.

\begin{pseudocode}[tabsize=1em]
    \Procedure<Tokenize L4CQUER Data>(
    \Argument{input}<istream>[a stream of characters](the characters of input to be tokenized)
)
    \NewKeywords{is,not,output,each}

    \Set{previous EOL}=no. \Comment{flag for whether the last output was an EOL token}
    \Set{delimiter}=0. \Comment{current length of opening of quoted string or block comment}
    \Set{delimiter type}[delimitertype]=none.
    \Set{start index}[startidx]=nil. \Comment{will store the index of the start of a token}
    \Set{closing}=0. \Comment{current progress towards closing delimiter}

    \begin{For}(\each\ \DefVar{item}[itm], \DefVar{index} $\in$ \istream)
        \begin{If}(\delimiter\ \is\ \not\ empty) \Comment{check for comments or strings}
            \begin{If}(\itm\ \is\ \delimitertype)
                \begin{If}(\startidx\ $+$ \delimiter\ $=$ \index) \Comment{extend delimiter}
                    \Update{delimiter}=\delimiter\ $+1$.
                \end{If}
                \begin{ElseIf}(\delimitertype\ \is\ comment)
                    \begin{If}(\delimiter\ $\oldnot=$ 1) \Comment{handle block comments}
                        \Update{closing}=\closing\ $+1$.
                    \end{If}
                \end{ElseIf}
                \Statement TODO
            \end{If}
        \end{If}
        \Statement TODO: process \itm
    \end{For}
\end{pseudocode}

\end{appendices}

\end{document}

