#include "l4cquer.h"

#define L4CQUER_ERROR_NULL_TOKENIZER -1
#define L4CQUER_ERROR_NO_INPUT -2
#define L4CQUER_ERROR_END_OF_INPUT -3

static char tokenizer_category(struct l4cquer_tokenizer *tokenizer, char c) {
    if (c < 0 || c > 0x100)
        return '\0';
    return tokenizer->category[(unsigned char)c];
}

static void tokenizer_reset_state(struct l4cquer_tokenizer *tokenizer) {
    tokenizer->state.length = 0;
    tokenizer->state.start = 0;
    tokenizer->state.closing = 0;
    tokenizer->state.opening = 0;
}

static void tokenizer_load_state(struct l4cquer_tokenizer *tokenizer, const char *input, int inlen) {
    tokenizer->input = input;
    tokenizer->current = input;
    if (inlen)
        tokenizer->end = input + inlen;
    else
        for (tokenizer->end = input; *tokenizer->end; ++tokenizer->end);
    tokenizer->state.previous_eol = 0;
    tokenizer_reset_state(tokenizer);
}

static int tokenizer_continue_block_comment(struct l4cquer_tokenizer *tokenizer) {
    /* Not a comment character, just reset counter */
    if (tokenizer_category(tokenizer, *tokenizer->current++) != '\\') {
        tokenizer->state.length++;
        tokenizer->state.closing = 0;
        return 0;
    }

    /* Comment is closed, reset state! */
    if (tokenizer->state.closing == tokenizer->state.opening - 1) {
        tokenizer_reset_state(tokenizer);
        return 0;
    }

    /* No matter what we now extend the length */
    /* If we are just at the start, extend the opening, otherwise the closing */
    if (tokenizer->state.length++ == tokenizer->state.opening)
        tokenizer->state.opening++;
    else
        tokenizer->state.closing++;
    return 0;
}

static int tokenizer_continue_line_comment(struct l4cquer_tokenizer *tokenizer) {
    switch (tokenizer_category(tokenizer, *tokenizer->current++)) {
        case '\\':
            if (tokenizer->state.length++ == tokenizer->state.opening)
                tokenizer->state.opening++;
            break;

        case '\n':
            tokenizer_reset_state(tokenizer);
            break;

        default:
            tokenizer->state.length++;
    }

    return 0;
}

static int tokenizer_continue_comment(struct l4cquer_tokenizer *tokenizer) {
    if (tokenizer->state.opening > 1)
        return tokenizer_continue_block_comment(tokenizer);
    else
        return tokenizer_continue_line_comment(tokenizer);
}

static int tokenizer_continue_quoted_delimiter(struct l4cquer_tokenizer *tokenizer) {
    tokenizer->current++;
    /* Decide if the opening or closing should be extended */
    if (tokenizer->state.length++ == tokenizer->state.opening) {
        tokenizer->state.opening++;
    } else {
        /* If the quote is closed return it */
        tokenizer->state.closing++;
        if (tokenizer->state.closing == tokenizer->state.opening) {
            tokenizer->state.previous_eol = 0;
            return 1;
        }
    }
    return 0;
}

static int tokenizer_continue_quoted_other(struct l4cquer_tokenizer *tokenizer) {
    int length = tokenizer->state.length;
    if (length == tokenizer->state.opening && length % 2 == 0) {
        /* Empty string, return it but don't advance current since it will be
         * part of the next token */
        tokenizer->state.previous_eol = 0;
        return 1;
    }

    tokenizer->state.closing = 0;
    tokenizer->state.length++;
    tokenizer->current++;

    return 0;
}

static int tokenizer_continue_quoted(struct l4cquer_tokenizer *tokenizer) {
    char current = tokenizer_category(tokenizer, *tokenizer->current);
    char delimiter = tokenizer_category(tokenizer, *tokenizer->state.start);
    if (current == delimiter)
        return tokenizer_continue_quoted_delimiter(tokenizer);
    else
        return tokenizer_continue_quoted_other(tokenizer);
}

static int tokenizer_continue_bare(struct l4cquer_tokenizer *tokenizer) {
    char current = tokenizer_category(tokenizer, *tokenizer->current);
    char start = tokenizer_category(tokenizer, *tokenizer->state.start);

    if (current == start) {
        tokenizer->state.length++;
        tokenizer->current++;
        return 0;
    }

    /* Return token but don't advance as it will be part of the next token */
    tokenizer->state.previous_eol = 0;
    return 1;
}

static int tokenizer_continue_token(struct l4cquer_tokenizer *tokenizer) {
    switch (tokenizer_category(tokenizer, *tokenizer->state.start)) {
        case '\\':
            return tokenizer_continue_comment(tokenizer);

        case '\'':
        case '"':
        case '`':
            return tokenizer_continue_quoted(tokenizer);

        default:
            return tokenizer_continue_bare(tokenizer);
    }
}

static int tokenizer_start_token(struct l4cquer_tokenizer *tokenizer) {
    switch (tokenizer_category(tokenizer, *tokenizer->current)) {
        case '\'':
        case '"':
        case '`':
        case '\\':
            tokenizer->state.opening = 1;
            /* fall through */

        default:
            tokenizer->state.length = 1;
            tokenizer->state.start = tokenizer->current;
            /* fall through */

        case ' ':
            tokenizer->current++;
            return 0;

        case '\n':
            if (!tokenizer->state.previous_eol) {
                tokenizer->state.previous_eol = 1;
                tokenizer->state.length = 1;
                tokenizer->state.start = tokenizer->current++;
                return 1;
            } else {
                tokenizer->current++;
                return 0;
            }
    }
}

static int tokenizer_step_state(struct l4cquer_tokenizer *tokenizer) {
    if (tokenizer->state.length) {
        return tokenizer_continue_token(tokenizer);
    } else {
        return tokenizer_start_token(tokenizer);
    }
}

static const char *tokenizer_end_midstate(struct l4cquer_tokenizer *tokenizer, int *length) {
    if (tokenizer_category(tokenizer, *tokenizer->state.start) == '\\') {
        /* Do not output EOF */
        if (length) *length = L4CQUER_ERROR_END_OF_INPUT;
            return 0;
    } else {
        /* Output the token and prepare to output EOF */
        const char *result = tokenizer->state.start;
        if (length) *length = tokenizer->state.length;
        tokenizer_reset_state(tokenizer);
        tokenizer->state.previous_eol = 0;
        return result;
    }
}

static const char *tokenizer_end_state(struct l4cquer_tokenizer *tokenizer, int *length) {
    if (!tokenizer->state.length) {
        if (length) *length = 0;
        return tokenizer->current + 1;
    }

    return tokenizer_end_midstate(tokenizer, length);
}

const char *l4cquer_tokenize(struct l4cquer_tokenizer *tokenizer, const char *input, int inlen, int *length) {
#define REPORT_ERROR(ERR) do { if (length) *length = ERR; return 0; } while (0)
    if (!tokenizer) REPORT_ERROR(L4CQUER_ERROR_NULL_TOKENIZER);
    if (!input && !tokenizer->input) REPORT_ERROR(L4CQUER_ERROR_NO_INPUT);

    if (input && input != tokenizer->input)
        tokenizer_load_state(tokenizer, input, inlen);

    while (tokenizer->current < tokenizer->end) {
        if (tokenizer_step_state(tokenizer)) {
            const char *result = tokenizer->state.start;
            if (length) *length = tokenizer->state.length;
            tokenizer_reset_state(tokenizer);
            return result;
        }
    }

    return tokenizer_end_state(tokenizer, length);
}
