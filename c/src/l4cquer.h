/*!
 * \file l4cquer.h
 * \author Louis Burke
 *
 * \brief This header files defines an API for reading and writing L4CQUER
 * tokens.
 */

#ifndef L4CQUER_H
#define L4CQUER_H "0.1.1" /*!< Library version and single inclusion token */

/*!
 * \brief This structure represents the state of a L4CQUER tokenizer.
 *
 * This is initialized with the parameters of tokenization and maintains the
 * state of the tokenization process.
 *
 * The category interprets each unique character value as a separate category,
 * with ' ' for whitespace, "'` for primary, secondary, and tertiary quotation
 * marks respectively, '\n' for end of line, and '\\' for comment.
 *
 * The rest of the structure should be initialized to 0 at the start of parsing.
 */
struct l4cquer_tokenizer {
    char category[0x100]; /*!< The category of each character */

    const char *input; /*!< The input string */
    const char *current; /*!< Current start point */

    const char *end; /*!< End of input string (= input + length) */

    struct {
        const char *start; /*!< Start of current token */
        int length; /*!< Length of current token. */
        int opening; /*!< Length of the opening delimiter. */
        int closing; /*!< Length of current closing delimiter. */
        int previous_eol; /*!< Zero if the previously emitted token was not an end of line token */
    } state; /*!< Internal state of tokenizer. */
};

/*!
 * \brief This function outputs the next token of the input.
 *
 * The start of the token is returned as a pointer. The length of the token is
 * returned by a parameter.
 *
 * After an initial input specification, you may pass either NULL or the same
 * input to continue tokenization. Any other input will restart the
 * tokenization.
 *
 * On error, if length is non-null, it is loaded with a negative value
 * indicating the cause of error.
 *
 * \param[in]     tokenizer The tokenizer state to use for tokenization.
 * \param[in]     input     The input to tokenize, or continue tokenizing.
 * \param[in]     inlen     The length of the input, or 0 if null-terminated.
 * \param[out]    length    The length of the returned token.
 *
 * \return The start of the token, or NULL on error.
 */
const char *l4cquer_tokenize(struct l4cquer_tokenizer *tokenizer, const char *input, int inlen, int *length);

/* TODO: document, both behaviour and error numbers */
const char *l4cquer_error_message(int error);

#endif /* L4CQUER_H */
