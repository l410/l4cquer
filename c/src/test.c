#include <stdio.h>
#include <unistd.h>

#include "l4cquer.h"

#define BUFSIZE 0x10000

int main(int argc, char *argv[]) {
    char buf[BUFSIZE];
    struct l4cquer_tokenizer tokenizer = {
        /*0123456789ABCDEF*/
         "??????????\n?????" /* 0 */
         "????????????????" /* 1 */
         " !\"!!!!'!!!!!!!!" /* 2 */
         "0000000000!\n'\\' " /* 3 */
         "!AAAAAAAAAAAAAAA" /* 4 */
         "AAAAAAAAAAA\"\\\"!B" /* 5 */
         "`aaaaaaaaaaaaaaa" /* 6 */
         "aaaaaaaaaaa`|`CD" /* 7 */
         "################" /* 8 */
         "################" /* 9 */
         "################" /* A */
         "################" /* B */
         "################" /* C */
         "################" /* D */
         "################" /* E */
         "################" /* F */
    };

    FILE *input = fopen(argv[1], "rb");

    int length = fread(buf, 1, BUFSIZE, input);

    fclose(input);

    for (;;) {
        int l;
        const char *token = l4cquer_tokenize(&tokenizer, buf, length, &l);

        if (token) {
            printf("%d: [", l);
            fflush(stdout);
            write(1, token, l);
            printf("]\n");
        }

        if (!l || !token)
            return 0;
    }
}
