.PHONY: docs

default: docs build

build: kotlin-build c-build

docs: l4cquer.pdf

kotlin-%:
	cd kotlin && gradle $*

c-%:
	cd c && $(MAKE) $*

%.pdf: %.tex
	arara $^

clean: clean-l4cquer.pdf kotlin-clean c-clean

clean-%.pdf:
	-rm -f $*.aux $*.idx $*.ilg $*.ind $*.log $*.out $*.toc
